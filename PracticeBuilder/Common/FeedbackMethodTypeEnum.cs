﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Common
{
    public enum FeedbackMethodTypeEnum
    {
         [Display(Name = "Email")]
         Email = 1,
        
        [Display(Name = "SMS")]
        SMS = 2
    }
}