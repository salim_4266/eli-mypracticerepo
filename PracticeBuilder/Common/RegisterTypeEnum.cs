﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Common
{
    public enum RegisterTypeEnum
    {
        [Display(Name = "Practice")]
        Practice = 1,
        
        [Display(Name = "Provider")]
        Provider = 2
    }
}