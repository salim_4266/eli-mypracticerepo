﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Common
{
    public enum SqlOperationType
    {
        Select = 1,
        Insert = 2,
        Update = 3,
        Delete = 4
        
    }
}