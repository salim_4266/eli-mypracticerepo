﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Microsoft.Owin.Security.Provider;
using PracticeBuilder.Common;

namespace PracticeBuilder.DAL
{
    public class SqlOperations
    {
        private readonly SqlConnection _dbConnection;
        private SqlCommand _dbCommand;
        public SqlOperations()
        {
            _dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["PracticeBuilderConnection"].ConnectionString);
        }

        public dynamic Execute(string query, SqlOperationType type)
        {
            dynamic result;
            using (_dbConnection)
            {
                var adapter = new SqlDataAdapter();
                _dbConnection.Open();
                ;
                _dbCommand = _dbConnection.CreateCommand();
                _dbCommand.CommandText = query;
               
                if (type == SqlOperationType.Insert)
                {
                     adapter.InsertCommand = _dbCommand;
                     adapter.InsertCommand.ExecuteNonQuery();
                    result = 1;
                }
                else
                {
                    result = new DataTable();
                    adapter.SelectCommand = _dbCommand;
                    adapter.Fill(result);                   
                }

            }

            return result;
        }
    }
}