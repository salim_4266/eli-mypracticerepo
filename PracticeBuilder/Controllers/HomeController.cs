﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Owin.Security.Twitter.Messages;
using Newtonsoft.Json.Serialization;
using PracticeBuilder.Common;
using PracticeBuilder.DAL;
using PracticeBuilder.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PracticeBuilder.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            //Logging the Exception
            filterContext.ExceptionHandled = true;


            var result = this.View("Error", new HandleErrorInfo(exception,
                filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));

            filterContext.Result = result;

        }
        //
        // GET: /Home/
        public ActionResult FeedbackDashboard()
        {
            var lstOfProviders = new ProviderListViewModel();
            try
            {
                if (Session["PracticeID"] != null)
                {
                    var operation = new SqlOperations();
                    var query = string.Format("SELECT ProviderId, FirstName +' '+ LastName AS Name FROM pbr.Providers WHERE practiceId = {0} AND IsActive = 1 ", Session["PracticeID"] ?? "");
                    var dtProvider = (DataTable)operation.Execute(query, SqlOperationType.Select);
                    if (dtProvider.Rows.Count > 0)
                    {
                        var lstProviders = new List<UserViewModel>();
                        foreach (var row in dtProvider.Rows.Cast<DataRow>())
                        {
                            var provider = new UserViewModel();
                            provider.ProviderId = Convert.ToInt32(row["ProviderId"]);
                            provider.Name = Convert.ToString(row["Name"]);
                            lstProviders.Add(provider);
                        }
                        lstOfProviders.ListOfProviders = new SelectList(lstProviders, "ProviderId", "Name");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return View(lstOfProviders);
        }

        public ActionResult FeedbackSubmit(string providerId)
        {
            try
            {

                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                    new KeyValuePair<string, string>("secretKey",
                        ConfigurationManager.AppSettings["secretKey"]),
                    new KeyValuePair<string, string>("userId", providerId)
                });

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    var response =
                        client.PostAsync("myprac1/public/api/provider/get/param/list", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;

                        if (jsonStr != null)
                        {
                            ViewBag.ProviderId = providerId;
                            ViewBag.Url = jsonStr.providers[0].url;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }


            return View(ViewBag);
        }

        public ActionResult Logout()
        {
            if (Session["Username"] != null)
            {
                //redirect to the page where you want for example
                return View();
            }
            else
            {
                //Redirect to the login action for example
                return RedirectToAction("Login");
            }
        }
        public ActionResult FeedbackOptions(string Id)
        {
            ViewBag.PublisherId = Id;
            return View();
        }

        [HttpPost]
        public ActionResult FeedbackOptions(FeedbackMethodViewModel feedback)
        {
            if (ModelState.IsValid)
            {
                string PublisherId = "";
                if (feedback.PublisherId == "")
                {
                    PublisherId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                }
                else
                {
                    PublisherId = feedback.PublisherId;
                }

                if (feedback.FeedbackMethodType == FeedbackMethodTypeEnum.Email)
                {
                    try
                    {
                        var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                            new KeyValuePair<string, string>("secretKey",
                                ConfigurationManager.AppSettings["secretKey"]),
                            new KeyValuePair<string, string>("userId", PublisherId),
                            new KeyValuePair<string, string>("firstName", feedback.PatientFName),
                            new KeyValuePair<string, string>("lastName", feedback.PatientLName),
                            new KeyValuePair<string, string>("email", feedback.PatientEmailaddress)

                        });

                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                            client.DefaultRequestHeaders.Accept.Clear();

                            var response =
                                client.PostAsync("myprac1/public/api/patient/post/param/feedback", content).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                                ViewBag.Message = "Email has been sent Successfully!"; //jsonStr.message;
                            }
                        }
                    }

                    catch (Exception e)
                    {

                        throw e;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(feedback.PatientPhone))
                    {
                        ModelState.AddModelError("PatientPhone", "Phone number is mandatory");
                    }
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", PublisherId),
                                new KeyValuePair<string, string>("firstName", feedback.PatientFName),
                                new KeyValuePair<string, string>("lastName", feedback.PatientLName),
                                new KeyValuePair<string, string>("email", feedback.PatientEmailaddress),
                                new KeyValuePair<string, string>("phone", feedback.PatientPhone)

                            });

                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                                client.DefaultRequestHeaders.Accept.Clear();

                                var response =
                                    client.PostAsync("myprac1/public/api/sms/post/param/feedback", content).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                                    ViewBag.Message = "SMS has been sent Successfully!";
                                }
                            }
                        }
                        catch (Exception e)
                        {

                            throw e;
                        }
                    }
                }
            }
            return View(feedback);
        }

        public ActionResult FeedbackPublish(string PublisherId)
        {
            var listReviews = new List<FeedbackPublishViewModel>();

            try
            {
                string TempUserId = "";
                if (PublisherId == null)
                {
                    TempUserId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                }
                else
                {
                    TempUserId = PublisherId.ToString();
                }

                var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", TempUserId),
                                new KeyValuePair<string, string>("reviewType", "new")                                
                            });

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                    client.DefaultRequestHeaders.Accept.Clear();

                    var response = client.PostAsync("myprac1/public/api/review/get/param/review", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                        listReviews = jsonStr.reviews;
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }

            return View(listReviews);
        }

        [HttpPost]
        public JsonResult DeleteFeedback(int feedbackId)
        {

            try
            {
                var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0]),
                                new KeyValuePair<string, string>("reviewId", feedbackId.ToString())                                
                            });

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                    client.DefaultRequestHeaders.Accept.Clear();

                    var response =
                        client.PostAsync("myprac1/public/api/review/delete/param/review", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                        if (jsonStr.status == "401")
                        {
                            throw new Exception(jsonStr.message);
                        }
                        return Json(new
                        {
                            status = jsonStr.status,
                            message = jsonStr.message
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return new JsonResult();
        }

        [HttpPost]
        public JsonResult PublishFeedback(int feedbackId)
        {
            try
            {
                var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0]),
                                new KeyValuePair<string, string>("reviewId", feedbackId.ToString())                                
                            });

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("accept", "Application/JSON");
                    var response = client.PostAsync("myprac1/public/api/review/put/param/publish", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                        return Json(new
                              {
                                  status = jsonStr.status,
                                  message = jsonStr.message
                              }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                //throw e;
            }
            return new JsonResult();
        }

        public ActionResult FeedbackReport(string ReportType, string PublisherId)
        {
            if (ReportType.ToLower() == "sites")
            {
                var listReportReviews = new List<ProviderSitesViewModel>();
                try
                {
                    string TempUserId = "";
                    if (PublisherId == null || PublisherId == "0")
                    {
                        TempUserId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                    }
                    else
                    {
                        TempUserId = PublisherId.ToString();
                    }

                    var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", TempUserId),
                            });

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        var response = client.PostAsync("myprac1/public/api/review/get/param/sites", content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonStr = response.Content.ReadAsAsync<ReportSiteViewModel>().Result;
                            listReportReviews = jsonStr.list;
                            if(listReportReviews != null)
                            {
                                if (listReportReviews.Count > 0)
                                    ViewBag.SiteReports = listReportReviews;
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
            else if (ReportType.ToLower() == "sms")
            {
                var listReportReviews = new List<ProviderSMSViewModel>();
                try
                {
                    string TempUserId = "";
                    if (PublisherId == null || PublisherId == "0")
                    {
                        TempUserId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                    }
                    else
                    {
                        TempUserId = PublisherId.ToString();
                    }

                    var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", TempUserId),
                            });

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        var response = client.PostAsync("myprac1/public/api/reports/get/param/sms", content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonStr = response.Content.ReadAsAsync<ReportSMSViewModel>().Result;
                            listReportReviews = jsonStr.report;
                            if (listReportReviews != null)
                            {
                                if (listReportReviews.Count > 0)
                                    ViewBag.SMSReports = listReportReviews;
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
            else if (ReportType.ToLower() == "pqueue")
            {
                var listReportReviews = new List<ProviderPublishingQueueViewModel>();
                try
                {
                    string TempUserId = "";
                    if (PublisherId == null || PublisherId == "0")
                    {
                        TempUserId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                    }
                    else
                    {
                        TempUserId = PublisherId.ToString();
                    }

                    var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", TempUserId),
                                new KeyValuePair<string, string>("reviewType", "publishing"),
                            });

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        var response = client.PostAsync("myprac1/public/api/review/get/param/review", content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var jsonStr = response.Content.ReadAsAsync<ReportPublishingQueueViewModel>().Result;
                            listReportReviews = jsonStr.reviews;
                            if (listReportReviews != null)
                            {
                                if (listReportReviews.Count > 0)
                                    ViewBag.PublishingReport = listReportReviews;
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
            else
            {
                var listReportReviews = new List<ProviderReportViewModel>();
                try
                {
                    string TempUserId = "";
                    if (PublisherId == null || PublisherId == "0")
                    {
                        TempUserId = System.Web.HttpContext.Current.User.Identity.Name.Split('~')[0];
                    }
                    else
                    {
                        TempUserId = PublisherId.ToString();
                    }

                    var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("userId", TempUserId),
                            });

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        if (ReportType == "feedback")
                        {
                            var response = client.PostAsync("myprac1/public/api/reports/get/param/feedback", content).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var jsonStr = response.Content.ReadAsAsync<ReportViewModel>().Result;
                                listReportReviews = jsonStr.report;
                                if (listReportReviews != null)
                                {
                                    if (listReportReviews.Count > 0)
                                        ViewBag.Reports = listReportReviews;
                                }
                            }
                        }
                        else
                        {
                            var response = client.PostAsync("myprac1/public/api/reports/get/param/published", content).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                var jsonStr = response.Content.ReadAsAsync<ReportViewModel>().Result;
                                listReportReviews = jsonStr.report;
                                if (listReportReviews != null)
                                {
                                    if (listReportReviews.Count > 0)
                                        ViewBag.Reports = listReportReviews;
                                }
                            }
                        }

                    }
                }
                catch (Exception e)
                {

                    throw e;
                }

            }
            return View();
        }
    }
}