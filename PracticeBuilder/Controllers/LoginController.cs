﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Security;
using PracticeBuilder.Common;
using PracticeBuilder.DAL;
using PracticeBuilder.Models;
using System.Data.SqlClient;

namespace PracticeBuilder.Controllers
{
    public class LoginController : Controller
    {

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            //Logging the Exception
            filterContext.ExceptionHandled = true;


            var result = this.View("Error", new HandleErrorInfo(exception,
                filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));

            filterContext.Result = result;

        }

        public ActionResult Signup()
        {            
            return View();
        }

        [HttpPost]
        public ActionResult Signup(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                if (register.RegistrationType == RegisterTypeEnum.Practice)
                {
                    if (string.IsNullOrEmpty(register.Name))
                    {
                        ModelState.AddModelError("Name", "You must fill in Practice Name.");
                    }
                    if (ModelState.IsValid)
                    {
                        var operation = new SqlOperations();

                        try
                        {
                            int practiceId = 0;
                            var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("address1", register.Address1),
                                new KeyValuePair<string, string>("address2", register.Address2),
                                new KeyValuePair<string, string>("businessName", register.Name),
                                new KeyValuePair<string, string>("city", register.City),
                                new KeyValuePair<string, string>("state", register.State),
                                new KeyValuePair<string, string>("email", register.Email),
                                new KeyValuePair<string, string>("telephone", register.Telephone),
                                new KeyValuePair<string, string>("fax", register.Fax),
                                new KeyValuePair<string, string>("zip", register.Zip),
                                new KeyValuePair<string, string>("businessUnit", register.BUName)

                            });

                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                                client.DefaultRequestHeaders.Accept.Clear();

                                var response =
                                    client.PostAsync("myprac1/public/api/clinic/post/param/add", content).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                                    if (jsonStr != null)
                                    {
                                        practiceId = Convert.ToInt32(jsonStr.userId);
                                        var registerPracticeQuery =
                                            string.Format(
                                                "INSERT INTO pbr.Practices(PracticeId, PracticeName, Address1, Address2, City, State, ZipCode, PhoneNumber, Fax, Email)" +
                                                " VALUES ('{0}','{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}','{8}', '{9}' )",
                                                practiceId, register.Name, register.Address1, register.Address2,
                                                register.City, register.State, register.Zip, register.Telephone,
                                                register.Fax, register.Email);

                                        operation.Execute(registerPracticeQuery, SqlOperationType.Insert);
                                    }
                                }
                            }

                            TempData["Message"] =
                                "Your Practice has been registered successfully. Your Practice registeration number is " +
                               practiceId + ". Please note down the Id as it will be used for future references.";
                            //register.PracticeId will come from the API once we call the API to save the practice's information                            
                            return RedirectToAction("SignupSuccess", "Login");
                        }
                        catch (Exception e)
                        {

                            throw e;
                        }
                    }

                }
                else
                {
                    if (string.IsNullOrEmpty(register.FirstName))
                    {
                        ModelState.AddModelError("FirstName", "FirstName is mandatory");
                    }

                    if (string.IsNullOrEmpty(register.LastName))
                    {
                        ModelState.AddModelError("LastName", "LastName is mandatory");
                    }

                    if (string.IsNullOrEmpty(register.NPI))
                    {
                        ModelState.AddModelError("NPI", "NPI is mandatory");
                    }

                    if (string.IsNullOrEmpty(register.NPI))
                    {
                        ModelState.AddModelError("NPI", "NPI is mandatory");
                    }

                    if (string.IsNullOrEmpty(register.ProviderPId))
                    {
                        ModelState.AddModelError("ProviderPId", "PersonalId is mandatory");
                    }

                    if (!register.PracticeId.HasValue)
                    {
                        ModelState.AddModelError("PracticeId", "PracticeId is mandatory");
                    }

                    if (!register.PracticeId.HasValue)
                    {
                        ModelState.AddModelError("PracticeId", "Please enter a valid ID");
                    }
                    if (ModelState.IsValid)
                    {
                        var operation = new SqlOperations();

                        try
                        {
                            var proceedWithSave = false;
                            var content = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("authKey", ConfigurationManager.AppSettings["authKey"]),
                                new KeyValuePair<string, string>("secretKey",
                                    ConfigurationManager.AppSettings["secretKey"]),
                                new KeyValuePair<string, string>("npi", register.NPI)
                            });

                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                                client.DefaultRequestHeaders.Accept.Clear();
                                var response =
                                    client.PostAsync("myprac1/public/api/provider/get/param/list", content).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    var jsonStr = response.Content.ReadAsStringAsync().Result;
                                    if (jsonStr.IndexOf("\"providers\":[]") > 0)
                                    {
                                        proceedWithSave = true;
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("NPI", "This npi number already exists.");
                                    }
                                }

                            }
                            if (proceedWithSave)
                            {
                                content = new FormUrlEncodedContent(new[]
                                {
                                    new KeyValuePair<string, string>("authKey",
                                        ConfigurationManager.AppSettings["authKey"]),
                                    new KeyValuePair<string, string>("secretKey",
                                        ConfigurationManager.AppSettings["secretKey"]),
                                    new KeyValuePair<string, string>("npi", register.NPI),
                                    new KeyValuePair<string, string>("clinicId", register.PracticeId.Value.ToString()),
                                    new KeyValuePair<string, string>("firstName", register.FirstName),
                                    new KeyValuePair<string, string>("lastName", register.LastName),
                                    new KeyValuePair<string, string>("middleName", register.MiddleName),
                                    new KeyValuePair<string, string>("email", register.Email),
                                    new KeyValuePair<string, string>("credentialId", "1"),
                                    //sending M.D. always as it is of No use and slowing down the page                     
                                    new KeyValuePair<string, string>("address1", register.Address1),
                                    new KeyValuePair<string, string>("address2", register.Address2),
                                    new KeyValuePair<string, string>("city", register.City),
                                    new KeyValuePair<string, string>("state", register.State),
                                    new KeyValuePair<string, string>("zip", register.Zip),
                                    new KeyValuePair<string, string>("telephone", register.Telephone),
                                    new KeyValuePair<string, string>("fax", register.Fax),
                                    new KeyValuePair<string, string>("businessUnit", register.BUName)
                                    //,
                                   // new KeyValuePair<string, bool>("SuperAdmin", register.SuperAdmin)

                                });

                                using (var client = new HttpClient())
                                {
                                    client.BaseAddress = new Uri("http://stage.expertpracticemarketing.com/");
                                    client.DefaultRequestHeaders.Accept.Clear();

                                    var response =
                                        client.PostAsync("myprac1/public/api/provider/post/param/add", content).Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var jsonStr = response.Content.ReadAsAsync<ResponseViewModel>().Result;
                                        if (jsonStr != null)
                                        {
                                            var registerPracticeQuery =
                                                string.Format(
                                                    "INSERT INTO pbr.Providers(ProviderId, PersonalId, FirstName, MiddleName, LastName, NPI, Degree, PracticeId, Address1, Address2, City, State, ZipCode, PhoneNumber, Fax, Email,IsSuperAdmin, IsActive )" +
                                                    " VALUES ('{0}','{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}','{8}', '{9}','{10}','{11}', '{12}', '{13}', '{14}','{15}','{16}', 1)",
                                                    jsonStr.userId
                                                    , register.ProviderPId, register.FirstName,
                                                    register.MiddleName, register.LastName, register.NPI, "M.D.",
                                                    register.PracticeId, register.Address1, register.Address2,
                                                    register.City, register.State, register.Zip, register.Telephone,
                                                    register.Fax, register.Email, register.SuperAdmin);

                                            operation.Execute(registerPracticeQuery, SqlOperationType.Insert);
                                        }
                                        //                            else
                                        //                            (model.SuperAdmin == true)
                                        //{
                                        //    response.Content.ReadAsAsync(SuperAdmin.Id, "SuperAdmin");
                                        //}



                                    }
                                }
                                TempData["Message"] =
                                    "Provider has been registered successfully. Please use your Personal Id to login.";
                                return RedirectToAction("SignupSuccess", "Login");
                            }
                        }
                        catch (Exception e)
                        {

                            Response.Output.Write("Please enter a valid practice id",e);

                           //throw new Exception("please enter a valid",e);


                           
                        } 
                    }

                }
            }
            return View(register);
        }

        public ActionResult Signupsuccess()
        {
            return View();
        }

        
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]       
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                if (!login.PracticeId.HasValue)
                {
                    ModelState.AddModelError("PracticeId","Please fill in a valid Practice Id.");
                }

                if (!login.ProviderId.HasValue)
                {
                    ModelState.AddModelError("ProviderId", "Please fill in a valid Personal Id.");
                }

                if (ModelState.IsValid)
                {
                    
                    try
                    {
                        var operation = new SqlOperations();
                        var query = string.Format("SELECT * FROM pbr.Providers WHERE practiceId = {0} AND personalId = {1} AND IsActive = 1 ", login.PracticeId, login.ProviderId);
                        var dtProvider = (DataTable)operation.Execute(query, SqlOperationType.Select);
                        if (dtProvider.Rows.Count > 0)
                        {
                            var row = dtProvider.Rows[0];
                            var userName = Convert.ToString(row["ProviderId"]) + "~" +
                                           Convert.ToString(row["FirstName"]) + " " +
                                           Convert.ToString(row["LastName"]) + "~" +
                                           Convert.ToString(row["IsSuperAdmin"]);

                            Session["PracticeId"] = Convert.ToString(row["PracticeId"]);

                            FormsAuthentication.SetAuthCookie(userName, false);
                            if (Convert.ToBoolean(userName.Split('~')[2]))
                                return RedirectToAction("FeedbackDashboard", "Home");
                            else
                                return RedirectToAction("FeedbackPublish", "Home");
                        }
                        else 
                        {
                            ViewBag.LoginError = "Login failed, Please try again with valid credentials!";
                        }
                        
                    }
                    catch (Exception e)
                    {                        
                        throw e;
                    }
                   
                }                
            }

            return View("Login");
        }

        public ActionResult NewSigUp()
        {
            return View();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login");
        }
        

    }
}