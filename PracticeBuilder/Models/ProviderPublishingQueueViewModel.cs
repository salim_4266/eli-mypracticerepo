﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ProviderPublishingQueueViewModel
    {
        public string review_id = "";
        public string review = "";
        public string rating = "";
        public string site = "";
        public string patient_email = "";
        public string source = "";
    }
}