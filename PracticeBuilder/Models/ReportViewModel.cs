﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ReportViewModel
    {
        public string status = "";
        public List<ProviderReportViewModel> report { get; set; } 
    }
}