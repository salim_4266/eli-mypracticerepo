﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "You must fill in a valid Practice Id. ")]
        [Display(Name = "Practice Id")]
        public int? PracticeId { get; set; }

        [Required(ErrorMessage = "You must fill in a valid Personal Id. ")]
        [Display(Name = "Personal Id")]
        public int? ProviderId { get; set; }

    }
}