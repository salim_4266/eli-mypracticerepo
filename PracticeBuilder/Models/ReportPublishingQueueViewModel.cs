﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ReportPublishingQueueViewModel
    {
        public string status = "";
        public string count = "";
        public List<ProviderPublishingQueueViewModel> reviews = null;
    }
}