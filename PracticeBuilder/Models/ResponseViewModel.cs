﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Win32;

namespace PracticeBuilder.Models
{
    public class ResponseViewModel
    {
        public string status { get; set; }

        public string userId { get; set; }

        public List<ProviderResponseViewModel> providers { get; set; }

        public string message { get; set; }

        public int count { get; set; }

        public List<FeedbackPublishViewModel> reviews { get; set; }  
        
    }
}