﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class Util
    {
        public static string RoundOffRatings(string Rating)
        {
            if (Rating.Contains(".50"))
            {
                return Rating;
            }
            else
            {
                return Math.Round(Math.Round(Convert.ToDouble(Rating)), 1, MidpointRounding.AwayFromZero).ToString();
            }
        }

        public static string ChooseTitleByRatings(string Rating)
        {
            string ModifiedRating = "";
            if (Rating.Contains(".50"))
            {

            }
            else
            {
                ModifiedRating = Math.Round(Math.Round(Convert.ToDouble(Rating)), 1, MidpointRounding.AwayFromZero).ToString();
            }

            switch (ModifiedRating)
            {
                case "0" :
                    return "Bad";
                case "0.0":
                    return "Bad";
                case "0.5":
                    return "Bad";
                case "0.50":
                    return "Bad";
                case "1":
                    return "Poor";
                case "1.0":
                    return "Poor";
                case "1.5":
                    return "Poor";
                case "1.50":
                    return "Poor";
                case "2":
                    return "Fair";
                case "2.0":
                    return "Fair";
                case "2.5":
                    return "Fair";
                case "2.50":
                    return "Fair";
                case "3":
                    return "Average";
                case "3.0":
                    return "Average";
                case "3.5":
                    return "Average";
                case "3.50":
                    return "Average";
                case "4":
                    return "Good";
                case "4.0":
                    return "Good";
                case "4.5":
                    return "Good";
                case "4.50":
                    return "Good";
                case "5":
                    return "Excellent";
                case "5.0":
                    return "Excellent";
                default:
                    return "";
            }
        }
    }
}