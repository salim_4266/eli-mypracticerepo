﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PracticeBuilder.Common;

namespace PracticeBuilder.Models
{
    public class FeedbackMethodViewModel
    {
        //public string ProviderId { get; set; }

        [Display(Name = "PublisherId")]
        public string PublisherId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string PatientFName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string PatientLName { get; set; }

        [Required]
        [Display(Name = "E-mail Address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string PatientEmailaddress { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", ErrorMessage = "Phone number is not valid")]       
        public string PatientPhone { get; set; }

        public FeedbackMethodTypeEnum FeedbackMethodType { get; set; }
    }
}