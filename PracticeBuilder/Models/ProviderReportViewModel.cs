﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ProviderReportViewModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string requestedDate { get; set; }
        public string respondedDate { get; set; }
        public string status { get; set; }
    }
}