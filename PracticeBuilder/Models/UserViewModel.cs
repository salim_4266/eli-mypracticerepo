﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PracticeBuilder.Models
{
    public class ProviderListViewModel
    {   
       public int ProviderId { get; set; }
  
       [Display(Name="Select a provider")]
        public SelectList ListOfProviders { get; set; } 
    }

    public class UserViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int ProviderId { get; set; }

        public bool IsSuperAdmin { get; set; }

        public string Name { get; set; }
    }
}