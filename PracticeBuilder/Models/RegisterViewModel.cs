﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PracticeBuilder.Common;

namespace PracticeBuilder.Models
{
    public class RegisterViewModel
    {
        public int ProviderId { get; set; }
       
        [Display(Name="Personal Id")]
        public string ProviderPId { get; set; }

        public RegisterTypeEnum RegistrationType { get; set; }
        
        [Display(Name = "Practice Id")]
        public int? PracticeId { get; set; }

        [Display(Name = "Practice Name")]
        public string Name { get; set; }
       
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
      
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Display(Name = "NPI")]
        public string NPI { get; set; }

        //[Display(Name = "Degree")]
        //public List<DegreeModel> Degree { get; set; }

        [Required(ErrorMessage = "You must fill in Email address.")]
        [Display(Name = "E-mail")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "You must fill in Address1.")]
        [Display(Name = "Address1")]
        public string Address1 { get; set; }
        
        [Display(Name = "Address2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "You must fill in City.")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "You must fill in State.")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "You must fill in Zipcode.")]
        [Display(Name = "Zip")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "You must fill in Phone number.")]
        [Display(Name = "Telephone")]
        [RegularExpression(@"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", ErrorMessage = "Phone number is not valid")]       
        public string Telephone { get; set; }

        [Required(ErrorMessage = "You must fill in Fax.")]
        [Display(Name = "Fax")]
        [RegularExpression(@"^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$", ErrorMessage = "Fax number is not valid")]       
        public string Fax { get; set; }

        [Required(ErrorMessage = "You must fill in BU Name.")]
        [Display(Name = "Business Unit")]
        public string BUName { get; set; }

        [Required(ErrorMessage = "You must check in checkbox.")]
        [Display(Name = "SuperAdmin")]
        public bool SuperAdmin { get; set; }

    }
}