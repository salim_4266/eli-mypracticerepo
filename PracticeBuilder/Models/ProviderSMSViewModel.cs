﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ProviderSMSViewModel
    {
        public string name = "";
        public string mobile = "";
        public string requestedDate = "";
        public string status = "";
    }
}