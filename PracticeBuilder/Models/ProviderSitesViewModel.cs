﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ProviderSitesViewModel
    {
        public string siteID = "";
        public string siteName = "";
        public string siteUrl = "";
        public string rating = "";
        public string newReviews = "";
        public string totalReviews = "";
        public string lastUpdatedDate = "";
    }
}