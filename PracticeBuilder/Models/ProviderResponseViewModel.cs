﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Web;

namespace PracticeBuilder.Models
{
    public class ProviderResponseViewModel
    {
        public string status { get; set; }

        public string userId { get; set; }

        public string username { get; set; }

        public string npi { get; set; }

        public string displayName { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string fullName { get; set; }

        public string lastLoginDate { get; set; }

        public string url { get; set; }
    }
}