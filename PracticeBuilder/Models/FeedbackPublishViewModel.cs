﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PracticeBuilder.Models
{

    public class FeedbackPublishViewModel
    {
        
        public int reviewId { get; set; }

        [DisplayName("Review posted")]
        public string review { get; set; }

        [DisplayName("Review Date")]
        public string reviewDate { get; set; }

        [DisplayName("First Name")]
        public string patientFirstname { get; set; }

        [DisplayName("Last Name")]
        public string patientLastname { get; set; }

        [DisplayName("Email")]
        public string patientEmail { get; set; }

        [DisplayName("Facility Rating")]
        public string facilityRating { get; set; }

        [DisplayName("Staff Rating")]
        public string staffRating { get; set; }

        [DisplayName("Treatment Rating")]
        public string treatmentResultsRating { get; set; }

        [DisplayName("Provider Rating")]
        public string providerRating { get; set; }

        [DisplayName("Overall Rating")]
        public string overallRating { get; set; }
    }
}