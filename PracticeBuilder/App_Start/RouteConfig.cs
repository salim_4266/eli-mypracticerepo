﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PracticeBuilder
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Publish",
                url: "Home/FeedBackPublish/{Id}",
                defaults: new { controller = "Home", action = "FeedBackPublish", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Options",
                url: "Home/FeedbackOptions/{Id}",
                defaults: new { controller = "Home", action = "FeedbackOptions", Id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Reports",
                url: "Home/FeedBackReport/{ReportType}/{PublisherId}",
                defaults: new { controller = "Home", action = "FeedBackReport", ReportType = UrlParameter.Optional, PublisherId = UrlParameter.Optional }
            );
        }
    }
}
